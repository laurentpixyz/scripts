core.setInteractiveMode(True)

showEdges = core.askYesNo("Show Edges ?")
view.showEdges(showEdges)
view.refreshViewer()

showPolygons = core.askYesNo("Show Polygons ?")
view.showPolygons(showPolygons)
view.refreshViewer()

showBReps = core.askYesNo("Show BReps ?")
view.showBReps(showBReps)
view.refreshViewer()

showLines = core.askYesNo("Show Lines ?")
view.showLines(showLines)
view.refreshViewer()

showPoints = core.askYesNo("Show Points ?")
view.showPoints(showPoints)
view.refreshViewer()

showPatchesBorders = core.askYesNo("Show Patches Borders ?")
view.showPatchesBorders(showPatchesBorders)
view.refreshViewer()

showSkeleton = core.askYesNo("Show Skeleton ?")
view.showSkeleton(showSkeleton)
view.refreshViewer()