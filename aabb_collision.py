
def is_point_in_aabb(point3, aabb):
	if  point3.x >= aabb.low.x and point3.x <= aabb.high.x\
	and point3.y >= aabb.low.y and point3.y <= aabb.high.y\
	and point3.z >= aabb.low.z and point3.z <= aabb.high.z:
		return True
	else:
		return False

def get_aabb_corners(aabb):
	corner_1 = aabb.high
	corner_2 = aabb.low
	corner_3 = geom.Point3(corner_1.x, corner_1.y, corner_2.z)
	corner_4 = geom.Point3(corner_1.x, corner_2.y, corner_2.z)
	corner_5 = geom.Point3(corner_2.x, corner_1.y, corner_1.z)
	corner_6 = geom.Point3(corner_2.x, corner_2.y, corner_1.z)
	corner_7 = geom.Point3(corner_1.x, corner_2.y, corner_1.z)
	corner_8 = geom.Point3(corner_2.x, corner_1.y, corner_2.z)
	return [corner_1, corner_2, corner_3, corner_4, corner_5, corner_6, corner_7, corner_8]
	
	
def are_colliding(occurrence_1, occurrence_2):
	aabb_1 = scene.getAABB([occurrence_1])
	aabb_2 = scene.getAABB([occurrence_2])
	corners_1 = get_aabb_corners(aabb_1)
	corners_2 = get_aabb_corners(aabb_2)
	
	for corner in corners_1:
		if is_point_in_aabb(corner, aabb_2):
			return True
	for corner in corners_2:
		if is_point_in_aabb(corner, aabb_1):
			return True
	
	return False

def get_all_scene_collisions():
	overriding_material = material.createMaterial("Override", "color")
	core.setProperty(overriding_material, 'color', '[1.000000, 0.000000, 0.000000, 1.000000]')
	scene.setCurrentVariant(scene.addVariant("Collisions"))
	results = []
	parts = scene.getPartOccurrences()
	core.pushProgression(len(parts), "")
	core.removeLogFileVerbose(2)
	core.removeConsoleVerbose(2)
	for part_1 in parts:
		core.stepProgression()
		for part_2 in parts:
			if part_1 != part_2 and are_colliding(part_1, part_2):
				core.setProperty(part_1, 'Material', str(overriding_material))
				results.append([part_1, part_2])
	core.addLogFileVerbose(2)
	core.addConsoleVerbose(2)
	return results
				
				
print(get_all_scene_collisions())