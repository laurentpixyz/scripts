import math

def vector(pt1, pt2):
	return Point3(pt2.x - pt1.x, pt2.y - pt1.y, pt2.z - pt1.z)

def angle(v1, v2):
	# return radians
	return math.acos(dot_product(v1, v2)/(vector_length(v1)*vector_length(v2)))

def rad_to_deg(rad):
	return rad * 180/math.pi

def dot_product(pt1, pt2):
	return pt1.x * pt2.x + pt1.y * pt2.y + pt1.z * pt2.z
	
def vector_length(v):
	return math.sqrt(dot_product(v, v))

def center(pt1, pt2):
	return Point3((pt1.x + pt2.x)/2, (pt1.y + pt2.y)/2, (pt1.z + pt2.z)/2)

def compute_delta(a, b, c):
	return b*b-4*a*c
	
def solver2(a, b, c):
	delta = compute_delta(a, b, c)
	if delta > 0:
		sqrt_delta = math.sqrt(delta)
		values = [(-b-sqrt_delta)/(2*a),(-b+sqrt_delta)/(2*a)]
	elif delta < 0:
		values = []
	else:
		values = [-b/(2*a)]
		return values

def get_global_matrix(global_matrix, local_matrix):
	# slooooow
	return geom.multiplyMatrices(geom.invertMatrix(global_matrix), local_matrix)

def multiplyMatrices(X, Y):
	return [[sum(a*b for a,b in zip(X_row,Y_col)) for Y_col in zip(*Y)] for X_row in X]

def debug_point(point, size):
	sphere = scene.createSphere(size, 16, 16, True)
	scene.setLocalMatrix(sphere, geom.fromTRS(point, pxz.geom.Point3(0,0,0), pxz.geom.Point3(1,1,1)))
	scene.setParent(sphere, scene.getRoot())

def bucket3d(root, n):
	aabb = scene.getAABB([root])
	origin = aabb.low
	high = aabb.high
	vector_aabb = vector(origin, high)
	
	step_x = int(vector_aabb.x / n)
	step_y = int(vector_aabb.y / n)
	step_z = int(vector_aabb.z / n)
	
	parts = scene.getPartOccurrences(root)
	chunks_parts = [[[list() for k in range(n)] for j in range(n)] for i in range(n)]
	
	for part in parts:
		part_aabb = scene.getAABB([part]) # costly
		center_part = center(part_aabb.high, part_aabb.low)
		origin_to_center = vector(origin, center_part)
		x = int(origin_to_center.x / step_x)
		y = int(origin_to_center.y / step_y)
		z = int(origin_to_center.z / step_z)
		chunks_parts[x][y][z].append(part)
	return chunks_parts
	
def bucket2d(root, n):
	aabb = scene.getAABB([root])
	origin = aabb.low
	high = aabb.high
	vector_aabb = vector(origin, high)
	
	step_x = int(vector_aabb.x / n)
	step_z = int(vector_aabb.z / n)
	
	parts = scene.getPartOccurrences(root)
	chunks_parts = [[list() for k in range(n)] for j in range(n)]
	
	core.removeConsoleVerbose(2)
	for part in parts:
		part_aabb = scene.getAABB([part]) # costly
		center_part = center(part_aabb.high, part_aabb.low)
		origin_to_center = vector(origin, center_part)
		x = math.floor(origin_to_center.x / step_x)
		z = math.floor(origin_to_center.z / step_z)
		try:
			chunks_parts[x][z].append(part)
		except:
			pass
	core.addConsoleVerbose(2)
	return chunks_parts

def get_peaks(points):
	x_pos = Point3(-float('Inf'), 0, 0)
	x_neg = Point3(float('Inf'), 0, 0)
	y_pos = Point3(0, -float('Inf'), 0)
	y_neg = Point3(0, float('Inf'), 0)
	z_pos = Point3(0, 0, -float('Inf'))
	z_neg = Point3(0, 0, float('Inf'))
	
	for point in points:
		if point.x >= x_pos.x:
			x_pos = point
		if point.y >= y_pos.y:
			y_pos = point
		if point.z >= z_pos.z:
			z_pos = point
		if point.x <= x_neg.x:
			x_neg = point
		if point.y <= y_neg.y:
			y_neg = point
		if point.z <= z_neg.z:
			z_neg = point
			
	return x_neg, y_neg, z_neg, x_pos, y_pos, z_pos
	
def split_list(alist, wanted_parts=1):
	length = len(alist)
	return [alist[i*length // wanted_parts: (i+1)*length // wanted_parts] for i in range(wanted_parts)]