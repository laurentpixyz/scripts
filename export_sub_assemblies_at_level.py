LEVEL = 3
FOLDER_PATH = r'C:\Users\nicol\Desktop\Temp\Coucou'
EXTENSION = 'fbx'

def find_assemblies_at_level(target_level, current_level, occurrence, assemblies):
	if target_level < current_level: return
	elif target_level == current_level:
		assemblies.append(occurrence)
	else:
		for child in scene.getChildren(occurrence):
			find_assemblies_at_level(target_level, current_level + 1, child, assemblies)
			

def export_sub_assemblies_at_level(target_level, extension, folder_path):
	assemblies = list()
	find_assemblies_at_level(target_level, 0, scene.getRoot(), assemblies)
	for assembly in assemblies:
		io.exportScene(folder_path + '/' + core.getProperty(assembly, 'Name') + '.' + extension, assembly)
	
export_sub_assemblies_at_level(LEVEL, EXTENSION, FOLDER_PATH)

