SCALE = 1/20   # Set the scale of your model
CONVERT_TO_Y_UP = True

MIN_SIZE = 150 # in mm
HOLES_DIAMETER = -1 # in mm

def center_model(root, convert_to_y_up = False):
	aabb = scene.getAABB([root])
	center = [(x + y)/2 for x, y in zip(aabb[0], aabb[1])]
	translationMatrix = [[1, 0, 0, -center[0]],
											 [0, 1, 0, -center[1]],
											 [0, 0, 1, -center[2]],
											 [0, 0, 0, 1]]
	for child in scene.getChildren(scene.getRoot()):
		scene.applyTransformation(child, translationMatrix)
		
	scaleMatrix = [[SCALE, 0, 0, 0],
								[0, SCALE, 0, 0],
								[0, 0, SCALE, 0],
								[0, 0, 0, 1]]
	for child in scene.getChildren(scene.getRoot()):
		scene.applyTransformation(child, scaleMatrix)		
		
		if convert_to_y_up:
			scene.applyTransformation(child, [[1, 0, 0, 0],
																		[0, 0, 1, 0],
																		[0, -1, 0, 0],
																		[0, 0, 0, 1]])
		
def deleteSmallParts():
	scene.selectByMaximumSize(MIN_SIZE, -1.000000)
	scene.deleteSelection()

def optimize(root):
	algo.repairMesh([root], 0.100000, True, False)
	algo.repairCAD([root], 0.100000, False)
	algo.tessellate([root], 0.200000, -1, -1, True, 0, 1, 0.000000, False, False, True, False)
	algo.removeHoles([root], True, True, True, HOLES_DIAMETER, 0)
	algo.decimate([root], 1.000000, -1, 8.000000, -1, False)
	deleteSmallParts()
	scene.mergeParts([root], 2)
	
def main(occurrence, convert_to_y_up):
	optimize(occurrence)
	center_model(occurrence, convert_to_y_up)

main(scene.getRoot(), CONVERT_TO_Y_UP)
	
	
