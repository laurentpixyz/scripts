root = 2
min_triangles = 5

def clean_hidden_removal(root):
	print('Cleaning hidden removal')
	core.removeConsoleVerbose(2)
	algo.explodeConnectedMeshes([root], min_triangles)
	parts_to_remove = list()
	parts = scene.getPartOccurrences(root)
	core.pushProgression(len(parts))
	for part in parts:
		core.stepProgression()
		if scene.getPolygonCount([part], True, True, True) < min_triangles:
			parts_to_remove.append(part)
	core.popProgression()
	scene.deleteOccurrences(parts_to_remove)
	scene.mergeFinalLevel([root], 2, True)
	core.addConsoleVerbose()
	
clean_hidden_removal(root, min_triangles)