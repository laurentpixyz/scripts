def vector(pt1, pt2):
	return Point3(pt2.x - pt1.x, pt2.y - pt1.y, pt2.z - pt1.z)

def dot_product(pt1, pt2):
	return pt1.x * pt2.x + pt1.y * pt2.y + pt1.z * pt2.z
	
def vector_length(v):
	return math.sqrt(dot_product(v, v))

resolution = 50

root = scene.getRoot()

algo.repairCAD([root], 0.100000, True)
algo.tessellate([root], 0.200000, -1, -1, True, 0, 1, 0.000000, False, False, True, False)

aabb = scene.getAABB([root])
size = vector_length(vector(aabb.high, aabb.low))

voxelSize = size/resolution
algo.smartOrient([root], voxelSize, 1, 64, 1, False)
