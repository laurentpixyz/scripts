import math

def point_x(pt):
	return pt[0]
	
def point_y(pt):
	return pt[1]

def point_z(pt):
	return pt[2]
	
def point_add(pt1, pt2):
	return [point_x(pt1)+point_x(pt2), point_y(pt1)+point_y(pt2), point_z(pt1)+point_z(pt2)]

def point_sub(pt1, pt2):
	return [point_x(pt1)-point_x(pt2), point_y(pt1)-point_y(pt2), point_z(pt1)-point_z(pt2)]

def dot_product(pt1,pt2):
	return point_x(pt1)*point_x(pt2) + point_y(pt1)*point_y(pt2) + point_z(pt1)*point_z(pt2)

def cross_product(pt1,pt2):
	return [point_y(pt1)*point_z(pt2) - point_z(pt1)*point_y(pt2), point_z(pt1)*point_x(pt2) - point_x(pt1)*point_z(pt2), point_x(pt1)*point_y(pt2) - point_y(pt1)*point_x(pt2)]

def vector_length(v):
	return math.sqrt(dot_product(v,v))

def get_size(occurrence):
	aabb = scene.getAABB([occurrence])
	return vector_length(point_sub(aabb[1], aabb[0]))

print(get_size(2))

scale = 10000/get_size(2)
print(scale)
		
scaleMatrix = [[scale, 0, 0, 0],
								[0, scale, 0, 0],
								[0, 0, scale, 0],
								[0, 0, 0, 1]]
								
scene.applyTransformation(2, scaleMatrix)	