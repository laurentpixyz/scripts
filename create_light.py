def createWhiteDirectionalLight(dir):
    # create a new occurrence
    lightOcc = scene.createOccurrence("Directional", scene.getRoot())    

    # add a Light component to the occurrence
    lightComp = scene.addComponent(lightOcc, scene.ComponentType.Light)

    # create a directional light
    dirLight = scene.createDirectionalLight(core.Color(1,1,1), 1.0, dir)

    # set the created light to the light component
    core.setProperty(lightComp, "Light", str(dirLight))

createWhiteDirectionalLight(geom.Point3(1, 0, 0))