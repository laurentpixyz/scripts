
def vector(pt1, pt2):
	return Point3(pt2.x - pt1.x, pt2.y - pt1.y, pt2.z - pt1.z)
	
def dot_product(pt1, pt2):
	return pt1.x * pt2.x + pt1.y * pt2.y + pt1.z * pt2.z
	
def vector_length(v):
	return math.sqrt(dot_product(v, v))

def center(pt1, pt2):
	return Point3((pt1.x + pt2.x)/2, (pt1.y + pt2.y)/2, (pt1.z + pt2.z)/2)

occurrence = 1
aabb = scene.getAABB([occurrence])
vect = vector(aabb.low, aabb.high)

print('Lowest point: ' + str(aabb.low))
print('Highest point: ' + str(aabb.high))
print('Center: ' + str(center(aabb.low, aabb.high)))
print('Length: ' + str(vector_length(vect)))