def vector(pt1, pt2):
	return Point3(pt2.x - pt1.x, pt2.y - pt1.y, pt2.z - pt1.z)

def vector_length(v):
	return math.sqrt(dot_product(v, v))

def center(pt1, pt2):
	return Point3((pt1.x + pt2.x)/2, (pt1.y + pt2.y)/2, (pt1.z + pt2.z)/2)

def getChunks(root, size):
	# bucket the root model with nxm millimeters squares
	aabb = scene.getAABB([root])
	origin = aabb.low
	high = aabb.high
	vector_aabb = vector(origin, high)
	
	n_step_x = int(vector_aabb.x / size) + 1
	n_step_y = int(vector_aabb.y / size) + 1
	n_step_z = int(vector_aabb.z / size) + 1
	
	parts = scene.getPartOccurrences(root)
	chunks_parts = [[[list() for x in range(n_step_x)] for y in range(n_step_y)] for z in range(n_step_z)]
	
	print('Bucketing...')

	core.removeConsoleVerbose(2)
	core.removeLogFileVerbose(2)
	core.removeSessionLogFileVerbose(2)

	for part in parts:
		part_aabb = scene.getAABB([part]) # costly
		center_part = center(part_aabb.high, part_aabb.low)
		origin_to_center = vector(origin, center_part)
		x = math.floor(origin_to_center.x / size)
		y = math.floor(origin_to_center.y / size)
		z = math.floor(origin_to_center.z / size)
		try:
			chunks_parts[x][y][z].append(part)
		except:
			pass
	
	core.addConsoleVerbose(2)
	core.addLogFileVerbose(2)
	core.addSessionLogFileVerbose(2)

	return chunks_parts

def mergeByChunks(root, size):
	for x in getChunks(root, size):
		for xy in x:
			for xyz in xy:
				if len(xyz) != 0:
					scene.mergeParts(xyz, 2)
					
mergeByChunks(scene.getRoot(), 10000)