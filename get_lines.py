def getLineOccurrences(root):
	core.configureInterfaceLogger(False, False, False)
	
	meshes = list()
	line_occurrences = list()
	parts = scene.getPartOccurrences(root)
	core.pushProgression(2 * len(parts))
	
	mesh_occurrences = list()
	for occurrence in parts:
		core.stepProgression()
		part = scene.getComponent(occurrence, pxz.scene.ComponentType.Part)
		try:
			mesh = scene.getPartMesh(part)
			meshes.append(mesh)
			mesh_occurrences.append(occurrence)
		except: # catch silently "no tessellated shape" error (means that the part was not tessellated)
			pass
	
	mesh_definitions = polygonal.getMeshDefinitions(meshes)
	vertices = list()
	for i in range(len(mesh_definitions)):
		core.stepProgression()
		mesh_def = mesh_definitions[i]
		if len(mesh_def.linesVertices) > 0 or len(mesh_def.lines) > 0:
			line_occurrences.append(mesh_occurrences[i])
	
	core.popProgression()
	core.configureInterfaceLogger(True, True, True)
	return line_occurrences

scene.select(getLineOccurrences(1))