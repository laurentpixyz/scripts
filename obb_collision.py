
def debug_point(point3, size=100, name='Sphere'):
	sphere = scene.createSphere(size, 16, 16, True)
	scene.setParent(sphere, scene.getRoot())
	core.setProperty(sphere, 'Transform', str(geom.fromTRS(pxz.geom.Point3(point3.x, point3.y, point3.z), pxz.geom.Point3(0, 0, 0), pxz.geom.Point3(1, 1, 1))))
	core.setProperty(sphere, 'Name', name)

def add_points(a, b):
	return geom.Point3(a.x + b.x, a.y + b.y, a.z + b.z)

def is_point_in_obb(point3, obb):
	debug_point(point3)
	if  point3.x >= obb.corner.x and point3.x <= add_points(obb.corner, obb.xAxis).x\
	and point3.y >= obb.corner.y and point3.y <= add_points(obb.corner, obb.yAxis).y\
	and point3.z >= obb.corner.z and point3.z <= add_points(obb.corner, obb.zAxis).z:
		return True
	else:
		return False

def get_obb_corners(obb):
	corner_1 = obb.corner
	corner_2 = add_points(corner_1, obb.xAxis)
	corner_3 = add_points(corner_1, obb.yAxis)
	corner_4 = add_points(corner_1, obb.zAxis)
	corner_5 = add_points(corner_2, obb.yAxis)
	corner_6 = add_points(corner_2, obb.zAxis)
	corner_7 = add_points(corner_3, obb.zAxis)
	corner_8 = add_points(corner_7, obb.xAxis)
	return [corner_1, corner_2, corner_3, corner_4, corner_5, corner_6, corner_7, corner_8]

def obb_collision(occurrence_1, occurrence_2):
	obb_1 = scene.getOBB([occurrence_1])
	obb_2 = scene.getOBB([occurrence_2])
	
	corners_1 = get_obb_corners(obb_1)
	corners_2 = get_obb_corners(obb_2)
	
	for corner in corners_1:
		#debug_point(corner)
		if is_point_in_obb(corner, obb_2):
			return True
	for corner in corners_2:
		if is_point_in_obb(corner, obb_1):
			return True
	
	return False

#print(obb_collision(2, 6))

def test(occurrence):
	obb = scene.getOBB([occurrence])
	#debug_point(obb.corner)
	#print(obb)
	corners = get_obb_corners(obb)
	for corner in corners:
		debug_point(corner)
test(2)	