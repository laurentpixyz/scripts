from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QObject, QThread, pyqtSignal, QEasingCurve, pyqtSignal
from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit, QFileDialog, QSizePolicy, QGraphicsDropShadowEffect, QVBoxLayout, QPushButton, QSlider

from PyQt5 import QtCore, QtGui, QtWidgets

class PiXYZThread(QObject):
	finished = pyqtSignal()
	
	def __init__(self):
		super(PiXYZThread, self).__init__()
	
	def run_pixyz_process(self):
		algo.decimate([1], 3.000000, -1, 15.000000, -1, False)
		self.finished.emit()

class ThreadHandler():
	def __init__(self):
		self.pixyz_thread = PiXYZThread()
		self.thread = QThread()
		self.pixyz_thread.moveToThread(self.thread)
		self.pixyz_thread.finished.connect(self.thread.quit)
		self.thread.started.connect(self.pixyz_thread.run_pixyz_process)
		self.thread.start()
		

def launch_thread():
	pixyz_thread = PiXYZThread()
	thread = QThread()
	pixyz_thread.moveToThread(thread)
	#pixyz_thread.finished.connect(thread.quit)
	thread.started.connect(pixyz_thread.run_pixyz_process)
	thread.start()

def set_visibility(value):
	print('Coucou')
	thread_handler = ThreadHandler()

customWidget = QWidget()
layout = QVBoxLayout()

booleanWidget = coregui.getValueWidget('Core', 'Boolean')
booleanWidget.clicked.connect(set_visibility)

point3Widget = coregui.getValueWidget('Geom', 'Point3')
# Custom button
button = QPushButton('Button')

button.clicked.connect(set_visibility)

layout.addWidget(button)
layout.addWidget(QSlider(QtCore.Qt.Horizontal))
layout.addWidget(booleanWidget)
layout.addWidget(point3Widget)
#layout.addWidget(coregui.getPropertyWidget(2, "Visible"))
customWidget.setLayout(layout)
coregui.dockWidget(customWidget, 'Custom PyQt Widget')