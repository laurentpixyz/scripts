def multiplyMatrices(X, Y):
	return [[sum(a*b for a,b in zip(X_row,Y_col)) for Y_col in zip(*Y)] for X_row in X]

def getVerticesGlobal(root):
	meshes = list()
	mesh_map_occurrence = dict()
	for occurrence in scene.getPartOccurrences(root):
		part = scene.getComponent(occurrence, pxz.scene.ComponentType.Part)
		mesh = scene.getPartMesh(part)
		meshes.append(mesh)
		mesh_map_occurrence[mesh] = occurrence

	mesh_definitions = polygonal.getMeshDefinitions(meshes)
	vertices = list()
	core.pushProgression(len(mesh_definitions))
	for mesh_def in mesh_definitions:
		core.stepProgression()
		occurrence = mesh_map_occurrence[mesh_def.id]
		global_matrix = scene.getGlobalMatrix(occurrence)
		part_local_matrix = eval(core.getProperty(part, 'Transform'))
		part_global_matrix = multiplyMatrices(global_matrix, part_local_matrix)
		for vertex in mesh_def.vertices:
			local_matrix = [[1, 0, 0, vertex.x], [0, 1, 0, vertex.y], [0, 0, 1, vertex.z], [0, 0, 0, 1]]
			matrix = multiplyMatrices(part_global_matrix, local_matrix)
			global_transform = Point3(matrix[0][3], matrix[1][3], matrix[2][3])
			vertices.append(global_transform)
	core.popProgression()

	return vertices

def getVertices(root):
	meshes = list()
	for occurrence in scene.getPartOccurrences(root):
		part = scene.getComponent(occurrence, pxz.scene.ComponentType.Part)
		mesh = scene.getPartMesh(part)
		meshes.append(mesh)

	mesh_definitions = polygonal.getMeshDefinitions(meshes)
	vertices = list()
	for mesh_def in mesh_definitions:
		vertices += mesh_def.vertices
	
	return vertices

print('\n')
print('\n' + str(getVerticesGlobal(1)))