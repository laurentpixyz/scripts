print('Begin')
core.configureInterfaceLogger(False, False, False)
parts = set(scene.getComponentByOccurrence(scene.getPartOccurrences(), 0, True))
count = 0
core.pushProgression(len(parts))
for part in parts:
    core.stepProgression()
    if part != 0:
        count += len(scene.listPartSubMaterials(part))
core.popProgression()
core.configureInterfaceLogger(True, True, True)
trianglesCount = scene.getPolygonCount([scene.getRoot()], True, True, False)
print('Meshes:' + str(count))
print('Unique Triangles: ' + str(trianglesCount))